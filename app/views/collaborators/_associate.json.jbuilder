json.extract! associate, :id, :created_at, :updated_at
json.url associate_url(associate, format: :json)
