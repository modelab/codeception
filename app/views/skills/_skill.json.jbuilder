json.extract! skill, :id, :associates_id, :certifications_id, :due_date, :created_at, :updated_at
json.url skill_url(skill, format: :json)
