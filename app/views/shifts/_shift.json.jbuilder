json.extract! shift, :id, :start_time, :end_time, :associates_number, :created_at, :updated_at
json.url shift_url(shift, format: :json)
