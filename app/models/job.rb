# == Schema Information
#
# Table name: jobs
#
#  id          :bigint           not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  location_id :bigint           not null
#

class Job < ApplicationRecord
  belongs_to :location, inverse_of: :jobs
  has_and_belongs_to_many :certifications
  has_many :shifts, inverse_of: :job
end
