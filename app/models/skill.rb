# == Schema Information
#
# Table name: skills
#
#  id               :bigint           not null, primary key
#  associate_id     :bigint
#  certification_id :bigint
#  due_date         :date
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Skill < ApplicationRecord
  belongs_to :associate, inverse_of: :skills
  belongs_to :certification, inverse_of: :skills

  validates :due_date, presence: true
end
