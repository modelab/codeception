# == Schema Information
#
# Table name: associates
#
#  id                     :bigint           not null, primary key
#  rut                    :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  first_name             :string
#  last_name              :string
#  cphone                 :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class Associate < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :authentication_keys => [:rut]

  has_many :schedules, inverse_of: :associate
  has_many :skills, inverse_of: :associate
  has_many :certifications, through: :skills, inverse_of: :associates
  has_many :messages, inverse_of: :associate

  validates :cphone, presence: true
  validates :cphone, length: { is: 9}
  validates :cphone, format: { with: /\d+/}

  def full_name
    [first_name, last_name].join(" ")
  end

end
