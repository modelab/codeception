# == Schema Information
#
# Table name: messages
#
#  id           :bigint           not null, primary key
#  shift_id     :bigint           not null
#  associate_id :bigint           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Message < ApplicationRecord
  belongs_to :associate, inverse_of: :messages
  belongs_to :shift, inverse_of: :messages
end
