# == Schema Information
#
# Table name: shifts
#
#  id                :bigint           not null, primary key
#  start_time        :datetime
#  end_time          :datetime
#  associates_number :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  job_id            :bigint
#

class Shift < ApplicationRecord
  has_many :schedules, inverse_of: :shift
  has_many :messages, inverse_of: :shift
  belongs_to :job, inverse_of: :shifts

  after_create :send_msg

  def self.allow_by_associate(associate)
    self.joins("LEFT JOIN schedules ON schedules.shift_id = shifts.id AND schedules.associate_id = #{associate.id}")
        .left_joins(job: { certifications: :skills })
        .where(schedules: { id: nil }, skills: { associate_id: associate.id })
  end

  private
    def send_msg
      associates = Associate.joins(:skills, :messages)
                            .where(skills: { certification_id: self.job.certifications.ids })
                            .where("messages.created_at > ?", (DateTime.now - 30.days))

      #associates.each { |asct| Whatsapp.send_shift(self, asct) }
      Whatsapp.send_shift(Shift.last, Associate.first) 
    end
end
