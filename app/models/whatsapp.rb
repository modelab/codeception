class Whatsapp
  def self.send_shift(shift, associate)
    client = Twilio::REST::Client.new(ENV["TWILIO_USER"], ENV["TWILIO_TOKEN"])

    message = client.messages.create(
      from: 'whatsapp:+14155238886',
      body: "*Nuevo turno para:*\n#{shift.job.name}\n\n_#{shift.job.description}_\n\nRegistrate ahora en el siguiente link:\nhttp://workmart.cl",
      #to: "whatsapp:+56#{associate.cphone}"
      to: "whatsapp:+56992240104"
    )

    puts message.sid
    associate.messages.create(shift: shift)
  end
end
