# == Schema Information
#
# Table name: schedules
#
#  id           :bigint           not null, primary key
#  associate_id :bigint
#  shift_id     :bigint
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Schedule < ApplicationRecord
  belongs_to :associate, inverse_of: :schedules
  belongs_to :shift, inverse_of: :schedules
end
