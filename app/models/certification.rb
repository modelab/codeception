# == Schema Information
#
# Table name: certifications
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Certification < ApplicationRecord
  has_many :skills, inverse_of: :certification
  has_many :associates, through: :skills, inverse_of: :certifications
  has_and_belongs_to_many :certifications
end
