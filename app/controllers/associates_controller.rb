class AssociatesController < ApplicationController
  before_action :authenticate_associate!

  def index
    @associate = current_associate
  end

end
