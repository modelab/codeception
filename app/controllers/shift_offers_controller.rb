class ShiftOffersController < ApplicationController
  before_action :authenticate_associate!

  def index
    @shifts = Shift.allow_by_associate(current_associate)
  end
end
