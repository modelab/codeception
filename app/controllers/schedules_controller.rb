class SchedulesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_associate!
  before_action :load_shift

  def create
    @schedule = current_associate.schedules.new(shift: @shift)
    respond_to do |format|
      if @schedule.save
        format.json { render json: :ok, status: :created }
      else
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @schedule = current_associate.schedules.find_by(shift: @shift)
    @schedule.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    def load_shift
      @shift = Shift.find(params[:shift_offer_id])
    end
end
