import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ["apply", "msg"]

  apply(elm) {
    if(confirm("Seguro?")) {
      fetch(`/shift_offers/${elm.target.dataset.shiftId}/schedules`, {
        method: "post"
      }).then((res) => {
        if(res.ok) {
          alert("Gracias por registrarte!!!");
          this.msgTarget.textContent = `Gracias!`;
          this.applyTarget.disabled = true;
        }
      }).catch(function(error) {
        alert('Hubo un problema con la petición Fetch: ' + error.message);
      });
    }
  }
}
