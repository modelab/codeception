import { Controller } from "stimulus"
import "materialize-css"

export default class extends Controller {
    static targets = []
    connect() {
        var select = document.querySelectorAll('select');
        M.FormSelect.init(select, {});
    }
}
