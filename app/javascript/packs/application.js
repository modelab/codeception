/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
// src/application.js

console.log('Hello World from Webpacker')
import './stylesheets/application'
import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import Turbolinks from 'turbolinks';
import 'materialize-css/dist/js/materialize'

Turbolinks.start();

const application = Application.start()
const context = require.context("./controllers", true, /\.js$/)
application.load(definitionsFromContext(context))


document.addEventListener("DOMContentLoaded", function (event) {
  var select = document.querySelectorAll('select');
  M.FormSelect.init(select, {});
  var dates = document.querySelectorAll('.datepicker');
  M.Datepicker.init(dates, {});
  var times = document.querySelectorAll('.timepicker');
  M.Timepicker.init(times, {});
  var sidenavs = document.querySelectorAll('.sidenav');
  M.Sidenav.init(sidenavs, {});
  var dropdowns = document.querySelectorAll('.dropdown-trigger');
  M.Dropdown.init(dropdowns, {});
});
