Rails.application.routes.draw do
  root "shift_offers#index"
  resources :locations
  resources :associates, only: [:index]
  resources :collaborators

  resources :skills, only: [:index]
  resources :certifications
  resources :jobs
  resources :shifts
  devise_for :admins
  devise_for :associates

  resources :shift_offers, only: [:index] do
    resources :schedules, only: [:create, :destroy], default: { format: :json }
  end
end
