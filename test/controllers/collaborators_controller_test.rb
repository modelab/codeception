require 'test_helper'

class CollaboratorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @associate = associates(:one)
  end

  test "should get index" do
    get _associates_url
    assert_response :success
  end

  test "should get new" do
    get new__associate_url
    assert_response :success
  end

  test "should create associate" do
    assert_difference('Associate.count') do
      post _associates_url, params: { associate: {  } }
    end

    assert_redirected_to associate_url(Associate.last)
  end

  test "should show associate" do
    get _associate_url(@associate)
    assert_response :success
  end

  test "should get edit" do
    get edit__associate_url(@associate)
    assert_response :success
  end

  test "should update associate" do
    patch _associate_url(@associate), params: { associate: {  } }
    assert_redirected_to associate_url(@associate)
  end

  test "should destroy associate" do
    assert_difference('Associate.count', -1) do
      delete _associate_url(@associate)
    end

    assert_redirected_to _associates_url
  end
end
