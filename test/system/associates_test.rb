require "application_system_test_case"

class AssociatesTest < ApplicationSystemTestCase
  setup do
    @associate = associates(:one)
  end

  test "visiting the index" do
    visit associates_url
    assert_selector "h1", text: "Associates"
  end

  test "creating a Associate" do
    visit associates_url
    click_on "New Associate"

    click_on "Create Associate"

    assert_text "Associate was successfully created"
    click_on "Back"
  end

  test "updating a Associate" do
    visit associates_url
    click_on "Edit", match: :first

    click_on "Update Associate"

    assert_text "Associate was successfully updated"
    click_on "Back"
  end

  test "destroying a Associate" do
    visit associates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Associate was successfully destroyed"
  end
end
