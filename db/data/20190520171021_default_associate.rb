class DefaultAssociate < SeedMigration::Migration
  def up
    Associate.create(rut: "1-9", password: "09090909", first_name: "Juan", last_name: "Lopez", cphone: "987654321")
  end

  def down
    Associate.find_by(rut: "1-9").try(:destroy)
  end
end
