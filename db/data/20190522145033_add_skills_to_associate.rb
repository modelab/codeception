class AddSkillsToAssociate < SeedMigration::Migration
  def up
    Associate.all.each do |a|
      Certification.all.each do |c|
        a.skills.new(certification: c, due_date: (Date.today + 30.days))
      end
      a.save
    end
  end

  def down

  end
end
