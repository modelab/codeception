class DefaultAdmin < SeedMigration::Migration
  def up
    Admin.create(email: "admin@walmart.com", password: "09090909", first_name: "First", last_name: "Admin")
  end

  def down
    Admin.find_by(email: "admin@walmart.com").try(:destroy)
  end
end
