class Shifts < SeedMigration::Migration
  def up
    location = Location.create(name: "CD 6011", address: "americo vespucio 3200, quilicura")

    job = Job.create(
      name: "Picker Flujo continuo",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      location: location
    )

    job.certifications.new(name: "Picker")

    job.shifts.new(
      start_time: DateTime.now,
      end_time: (DateTime.now + 8.hours),
      associates_number: 20
    )

    job.shifts.new(
      start_time: (DateTime.now + 1.day ),
      end_time: (DateTime.now + 1.day + 8.hours),
      associates_number: 20
    )

    job.shifts.new(
      start_time: (DateTime.now + 2.day ),
      end_time: (DateTime.now + 2.day + 8.hours),
      associates_number: 20
    )

    job.save
  end

  def down

  end
end
