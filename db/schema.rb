# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_22_174308) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "associates", force: :cascade do |t|
    t.string "rut", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.string "last_name"
    t.string "cphone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cphone"], name: "index_associates_on_cphone", unique: true
    t.index ["reset_password_token"], name: "index_associates_on_reset_password_token", unique: true
    t.index ["rut"], name: "index_associates_on_rut", unique: true
  end

  create_table "certifications", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "certifications_jobs", id: false, force: :cascade do |t|
    t.bigint "job_id", null: false
    t.bigint "certification_id", null: false
  end

  create_table "jobs", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "location_id", null: false
    t.index ["location_id"], name: "index_jobs_on_location_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "shift_id", null: false
    t.bigint "associate_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["associate_id"], name: "index_messages_on_associate_id"
    t.index ["shift_id"], name: "index_messages_on_shift_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.bigint "associate_id"
    t.bigint "shift_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["associate_id"], name: "index_schedules_on_associate_id"
    t.index ["shift_id"], name: "index_schedules_on_shift_id"
  end

  create_table "seed_migration_data_migrations", id: :serial, force: :cascade do |t|
    t.string "version"
    t.integer "runtime"
    t.datetime "migrated_on"
  end

  create_table "shifts", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer "associates_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "job_id"
    t.index ["job_id"], name: "index_shifts_on_job_id"
  end

  create_table "skills", force: :cascade do |t|
    t.bigint "associate_id"
    t.bigint "certification_id"
    t.date "due_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["associate_id"], name: "index_skills_on_associate_id"
    t.index ["certification_id"], name: "index_skills_on_certification_id"
  end

  add_foreign_key "certifications_jobs", "certifications"
  add_foreign_key "certifications_jobs", "jobs"
  add_foreign_key "jobs", "locations"
  add_foreign_key "messages", "associates"
  add_foreign_key "messages", "shifts"
  add_foreign_key "schedules", "associates"
  add_foreign_key "schedules", "shifts"
  add_foreign_key "shifts", "jobs"
  add_foreign_key "skills", "associates"
  add_foreign_key "skills", "certifications"
end
