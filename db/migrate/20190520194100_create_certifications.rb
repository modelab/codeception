class CreateCertifications < ActiveRecord::Migration[5.2]
  def change
    create_table :certifications do |t|
      t.string :name

      t.timestamps
    end
    create_join_table :jobs, :certifications
    add_foreign_key :certifications_jobs, :jobs
    add_foreign_key :certifications_jobs, :certifications
  end
end
