class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
    add_reference :shifts, :job, index: true
    add_foreign_key :shifts, :jobs

  end
end
