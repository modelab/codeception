class AddLocationToJobs < ActiveRecord::Migration[6.0]
  def change
    add_reference :jobs, :location, null: false, foreign_key: true, index: true
  end
end
