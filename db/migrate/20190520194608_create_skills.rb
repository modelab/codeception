class CreateSkills < ActiveRecord::Migration[5.2]
  def change
    create_table :skills do |t|
      t.references :associate, foreign_key: true
      t.references :certification, foreign_key: true
      t.date :due_date

      t.timestamps
    end
  end
end
